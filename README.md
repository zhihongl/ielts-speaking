# IELTS Speaking Android application #

Basicly, you need import the [Jsoup lib](http://jsoup.org/) to run this android application .

### What is this repository for? ###

* For IELTS Candidates
* Version 0.7

### How do I get set up? ###

* Fetch this project and Import Jsoup lib
* Dependencies only Jsoup
* Database is Sqlite which is required by Android
* In order to run test just in androidTest folder, which is mainly Junit test cases.
* Deployment is suitable for mobiles, I have not tested on tablets or watches

### Contribution guidelines ###

* Writing tests
* Code review
* Add new features

### Who do I talk to? ###

* Repo owner or admin
