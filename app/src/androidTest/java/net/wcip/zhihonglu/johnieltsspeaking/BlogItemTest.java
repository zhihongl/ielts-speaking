package net.wcip.zhihonglu.johnieltsspeaking;

import junit.framework.TestCase;

import net.wcip.zhihonglu.johnieltsspeaking.blog.BlogItem;
import net.wcip.zhihonglu.johnieltsspeaking.createtime.CreateTime;
import net.wcip.zhihonglu.johnieltsspeaking.partsection.PartSection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BlogItemTest extends TestCase {

    private BlogItem blogItem;

    @Before
    public void setUp() throws Exception {
        this.blogItem = new BlogItem("Sleeping", PartSection.PART_1,
                new CreateTime(2015, 03, 13, 17, 50),
                "http://blog.sina.com.cn/s/blog_79b6faea0102vk7c.html", "id", 5, 230);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testToString() {
        // System.out.println(this.blogItem.toString());

        assertEquals("Part 1 Sleeping\nhttp://blog.sina.com.cn/s/b"
                        + "log_79b6faea0102vk7c.html\n(5/230) 2015-3-13 17:50",
                this.blogItem.toString());

        this.blogItem.setTitle("title");
        assertEquals("title", this.blogItem.getTitle());

        this.blogItem.setPartSection(PartSection.PART_2);
        assertEquals(PartSection.PART_2, this.blogItem.getPartSection());

        this.blogItem.setCreateTime(new CreateTime(2015, 03, 13, 17, 50));
        assertEquals(new CreateTime(2015, 03, 13, 17, 50).toString(),
                this.blogItem.getCreateTime().toString());

        this.blogItem.setUrlAddress("http://title.com");
        assertEquals("http://title.com", this.blogItem.getUrlAddress());

        this.blogItem.setReadNum(56);
        assertEquals(56, this.blogItem.getReadNum());

        this.blogItem.setCommentsNum(256);
        assertEquals(256, this.blogItem.getCommentsNum());
    }

}
