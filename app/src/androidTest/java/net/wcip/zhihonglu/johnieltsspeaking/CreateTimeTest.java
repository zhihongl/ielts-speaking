package net.wcip.zhihonglu.johnieltsspeaking;

import net.wcip.zhihonglu.johnieltsspeaking.createtime.CreateTime;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CreateTimeTest {
	
	private CreateTime ct;

	@Before
	public void setUp() throws Exception {
		this.ct = new CreateTime(2015,12,12,21,11);
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToString() {
		System.out.println(ct.toString());
		assertEquals(2015, ct.getYear());
		assertEquals(12, ct.getMonth());
		assertEquals(12, ct.getDay());
		assertEquals(21, ct.getHour());
		assertEquals(11, ct.getMinute());
		assertEquals("2015-12-12 21:11", ct.toString());
		
		ct.setYear(2014);
		assertEquals(2014, ct.getYear());
		ct.setMonth(12);
		assertEquals(12, ct.getMonth());
		ct.setDay(23);
		assertEquals(23, ct.getDay());
		ct.setHour(22);
		assertEquals(22, ct.getHour());
		ct.setMinute(56);
		assertEquals(56, ct.getMinute());
		
	}

}
