package net.wcip.zhihonglu.johnieltsspeaking;

import java.util.ArrayList;
import java.util.Arrays;

import junit.framework.TestCase;

import net.wcip.zhihonglu.johnieltsspeaking.jsouphandler.JsoupHandler;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JsoupHandlerTest extends TestCase {

	private JsoupHandler jsoupHandler;
	
	private String testTitles[] ={"IELTS Speaking Part 1: Sleeping",
			"IELTS Speaking Part 1: Trees",
			"IELTS Speaking Part 1: Nearby Shops",
			"IELTS Speaking Part 1: Camping",
			"IELTS Speaking Part 1: The Sky",
			"IELTS Speaking Part 1: Friends",
			"IELTS Speaking Part 3 Buildings:",
			"IELTS Speaking Part 3: Journeys",
			"雅思口语 IELTS Speaking Part 3: Getting Lost"};

	@Before
	public void setUp() throws Exception {
		this.jsoupHandler = new JsoupHandler();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseTitleAndPartSection() {
		String _str = "IELTS Speaking Part 1: Sleeping topic ";
		String _results[] = { "Part 1", "Sleeping topic" };
		
		System.out.println("***"+this.jsoupHandler.parseTitleAndPartSection(_str));

//		assertEquals(_results[0],
//				this.jsoupHandler.parseTitleAndPartSection(_str)[0]);
//		assertEquals(_results[1],
//				this.jsoupHandler.parseTitleAndPartSection(_str)[1]);
	}
	
	public void testParseCreateTime() {
		String _str = "2015-03-03 17:05";
		
		System.out.println(Arrays.toString(this.jsoupHandler.parseCreateTime(_str)));
	}

}
