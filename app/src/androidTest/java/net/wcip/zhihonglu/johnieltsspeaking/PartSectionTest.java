package net.wcip.zhihonglu.johnieltsspeaking;

import net.wcip.zhihonglu.johnieltsspeaking.partsection.PartSection;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PartSectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSection() {
		assertEquals("Part 2", PartSection.PART_2.getSection());
		assertEquals("Part 1", PartSection.PART_1.getSection());
		assertEquals("Part 3", PartSection.PART_3.getSection());
	}

}
