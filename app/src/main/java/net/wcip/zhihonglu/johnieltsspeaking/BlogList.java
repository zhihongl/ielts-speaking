/* This is the activity for blog list, The design should be fetch data from
database, however currently we only fetch data from web.*/
package net.wcip.zhihonglu.johnieltsspeaking;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import net.wcip.zhihonglu.johnieltsspeaking.adapter.BlogAdapter;
import net.wcip.zhihonglu.johnieltsspeaking.blog.BlogItem;
import net.wcip.zhihonglu.johnieltsspeaking.jsouphandler.JsoupHandler;

import java.util.ArrayList;


public class BlogList extends ListActivity{

    // These are the Contacts rows that we will retrieve
    // This is the Adapter being used to display the list's data
    private BlogAdapter bAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setProgressDisplay();

        setContentView(R.layout.activity_blog_list);

        // For the cursor adapter, specify which columns go into which views
        String[] fromColumns = {"title", "createTime"};
        int[] toViews = {R.id.blogItemTitle, R.id.blogItemPublishTime}; // The TextView in

        ArrayList<BlogItem> _bloglists =  new ArrayList<BlogItem>();



        bAdapter = new BlogAdapter(this, _bloglists);

        setListAdapter(bAdapter);

        new MyAsyncTask().execute();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Uri uri = Uri.parse(((BlogItem)getListView().getItemAtPosition
                (position)).getUrlAddress());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }

    private void setProgressDisplay() {
        // Create a progress bar to display while the list loads
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.WRAP_CONTENT,
                RadioGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blog_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class MyAsyncTask extends AsyncTask<Void, BlogItem, Void> {

        private JsoupHandler jsoupHandler;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        this.jsoupHandler = new JsoupHandler();

        }

        @Override
        protected Void doInBackground(Void... params) {

            this.jsoupHandler.init();

            ArrayList<BlogItem> _bloglists = jsoupHandler.getBlogList();

            for (BlogItem each : _bloglists){
                publishProgress(each);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(BlogItem... values) {
            super.onProgressUpdate(values);

            bAdapter.add(values[0]);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}