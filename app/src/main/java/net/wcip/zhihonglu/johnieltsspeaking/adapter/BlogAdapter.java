package net.wcip.zhihonglu.johnieltsspeaking.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.wcip.zhihonglu.johnieltsspeaking.R;
import net.wcip.zhihonglu.johnieltsspeaking.blog.BlogItem;
import net.wcip.zhihonglu.johnieltsspeaking.config.GenerationConfig;
import net.wcip.zhihonglu.johnieltsspeaking.partsection.PartSection;

import java.util.ArrayList;

/**
 * Created by zhihongl on 17/03/2015.
 */
public class BlogAdapter extends ArrayAdapter<BlogItem> {

    private ArrayList<BlogItem> blogItemArrayList;
    private Context context;


    public BlogAdapter(Context context, ArrayList<BlogItem> blogItemArrayList) {
        super(context, R.layout.blog_item, R.id.blogItemTitle,
                blogItemArrayList);

        this.context = context;
        this.blogItemArrayList = blogItemArrayList;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(this
                .context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.blog_item, parent, false);

        TextView _title = (TextView) row.findViewById(R.id.blogItemTitle);
        TextView _createTime = (TextView) row.findViewById(R.id
                .blogItemPublishTime);
        ImageView _image = (ImageView) row.findViewById(R.id.imageView);


        BlogItem _temp = this.blogItemArrayList.get(position);

        Log.e(GenerationConfig.TAG, "Geted size = " + this.blogItemArrayList.size() + "");
        _title.setText(_temp.getTitle());
        _createTime.setText(_temp.getCreateTime().toString().split(" ")[0]);
        _image.setImageResource(getPartSection(_temp.getPartSection()));

//        _title.setText(titles[position]);
//        _createTime.setText(createTimes[position]);

        return row;
    }

    private int getPartSection(PartSection partSection) {
        if (partSection != null) {

            switch (partSection) {
                case PART_1:
                    return R.mipmap.part_1;
                case PART_2:
                    return R.mipmap.part_2;
                default:
                    return R.mipmap.part_3;
            }
        }
        return R.drawable.dot;
    }

}

