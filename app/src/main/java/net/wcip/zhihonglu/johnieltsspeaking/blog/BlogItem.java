package net.wcip.zhihonglu.johnieltsspeaking.blog;

import net.wcip.zhihonglu.johnieltsspeaking.createtime.CreateTime;
import net.wcip.zhihonglu.johnieltsspeaking.partsection.PartSection;

public class BlogItem {

    private String title;
    private PartSection partSection;
    private CreateTime createTime;
    private String urlAddress;
    private int commentsNum;
    private int readNum;
    private String id;

    public BlogItem() {
        // TODO Auto-generated constructor stub

    }

    public BlogItem(String title, PartSection partSection,
                    CreateTime createTime,
                    String urlAddress, String id) {
        this.title = title;
        this.partSection = partSection;
        this.createTime = createTime;
        this.urlAddress = urlAddress;
        this.id = id;
    }

    public BlogItem(String title, PartSection partSection, CreateTime createTime,
                    String urlAddress, String id, int commentsNum, int readNum) {
        this(title, partSection, createTime, urlAddress, id);
        this.commentsNum = commentsNum;
        this.readNum = readNum;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PartSection getPartSection() {
        return partSection;
    }

    public void setPartSection(PartSection partSection) {
        this.partSection = partSection;
    }

    public CreateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(CreateTime createTime) {
        this.createTime = createTime;
    }

    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    public int getCommentsNum() {
        return commentsNum;
    }

    public void setCommentsNum(int commentsNum) {
        this.commentsNum = commentsNum;
    }

    public int getReadNum() {
        return readNum;
    }

    public void setReadNum(int readNum) {
        this.readNum = readNum;
    }

    @Override
    public String toString() {
        // return partSection.getSection() + " " + title + "\n" + urlAddress
        // + "\n(" + commentsNum + "/" + readNum + ") "
        // + createTime.toString();
        return title + "\n" + urlAddress + "\n" + createTime.toString();
    }

}
