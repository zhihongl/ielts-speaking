package net.wcip.zhihonglu.johnieltsspeaking.config;

/**
 * Created by zhihongl on 19/03/2015.
 */
public class DatabaseConfiguration {

    public static final String DATABASE_NAME = "johnieltsspeakingdatabase.db";
    public static final int DATABASE_VERSION = 1;

    // table for Blog
    public static final String TABLE_BLOGS = "blogs";
    public static final String COLUMN_BLOG_ID = "_id";
    public static final String COLUMN_BLOG_NAME = "Name";

    // table for user's favourite
    public static final String TABLE_LIKES = "likes";
    public static final String COLUMN_LIKE_ID = COLUMN_BLOG_ID;
    public static final String COLUMN_LIKE_NAME = "Name";

    // create SQLite table
    public static final String CREATE_TABLE_BLOGS = "CREATE TABLE " +
            TABLE_BLOGS + "(" +
            COLUMN_BLOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_BLOG_NAME + " TEXT NOT NULL, " +
            ");";

    public static final String CREATE_TABLE_LIKES = "CREATE TABLE " +
            TABLE_BLOGS + "(" +
            COLUMN_LIKE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_LIKE_NAME + " INTEGER NOT NULL, " +
            ");";


}
