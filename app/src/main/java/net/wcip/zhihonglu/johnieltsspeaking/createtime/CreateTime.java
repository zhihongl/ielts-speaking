/* This object is used for holding Blog creating time*/
package net.wcip.zhihonglu.johnieltsspeaking.createtime;

import java.util.Calendar;

public class CreateTime {

    private Calendar createTime;

    public CreateTime() {
    }

    public CreateTime(int paras[]) {
        this(paras[0], paras[1], paras[2], paras[3], paras[4]);
    }

    public CreateTime(int year, int month, int date, int hours, int minutes) {

        createTime = Calendar.getInstance();
        this.createTime.set(year, month - 1, date, hours, minutes);

    }

    public int getYear() {
        return this.createTime.get(Calendar.YEAR);
    }

    public void setYear(int year) {
        this.createTime.set(Calendar.YEAR, year);
    }

    public int getMonth() {
        return this.createTime.get(Calendar.MONTH) + 1;
    }

    public void setMonth(int month) {
        this.createTime.set(Calendar.MONTH, month - 1);
    }

    public int getDay() {
        return this.createTime.get(Calendar.DATE);
    }

    public void setDay(int day) {
        this.createTime.set(Calendar.DATE, day);
    }

    public int getHour() {
        return this.createTime.get(Calendar.HOUR_OF_DAY);
    }

    public void setHour(int hour) {
        this.createTime.set(Calendar.HOUR_OF_DAY, hour);
    }

    public int getMinute() {
        return this.createTime.get(Calendar.MINUTE);
    }

    public void setMinute(int minute) {
        this.createTime.set(Calendar.MINUTE, minute);
    }

    @Override
    public String toString() {
        return "" + this.createTime.get(Calendar.YEAR) + "-"
                + (this.createTime.get(Calendar.MONTH) + 1) + "-"
                + this.createTime.get(Calendar.DATE) + " "
                + this.createTime.get(Calendar.HOUR_OF_DAY) + ":"
                + this.createTime.get(Calendar.MINUTE);
    }

}
