package net.wcip.zhihonglu.johnieltsspeaking.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.wcip.zhihonglu.johnieltsspeaking.config.DatabaseConfiguration;
import net.wcip.zhihonglu.johnieltsspeaking.config.GenerationConfig;

/**
 * Created by zhihongl on 19/03/2015.
 */
public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Context context) {
        super(context, DatabaseConfiguration.DATABASE_NAME, null,
                DatabaseConfiguration.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DatabaseConfiguration.CREATE_TABLE_BLOGS);
            db.execSQL(DatabaseConfiguration.CREATE_TABLE_LIKES);
        } catch (SQLException e) {
            Log.e(GenerationConfig.TAG, "" + e);
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // clear all tables
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConfiguration.TABLE_BLOGS);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConfiguration.TABLE_LIKES);

        // recreate new tables
        onCreate(db);
    }
}
