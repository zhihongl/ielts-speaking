package net.wcip.zhihonglu.johnieltsspeaking.jsouphandler;


import net.wcip.zhihonglu.johnieltsspeaking.blog.BlogItem;
import net.wcip.zhihonglu.johnieltsspeaking.createtime.CreateTime;
import net.wcip.zhihonglu.johnieltsspeaking.partsection.PartSection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsoupHandler {

    private static final String BLOG_LISTS_TOTAL = "div[class=articleCell SG_j_linedot1]";
    private Document webContent = null;
    private Elements blogLists;
    // John blog
    private String url = "http://blog.sina.com.cn/s/"
            + "articlelist_2042034922_0_1.html";

    public JsoupHandler() {
        // TODO Auto-generated constructor stub
    }

    public JsoupHandler(String url) {
        this.url = url;
    }

    public boolean init() {
        try {
            this.webContent = Jsoup.connect(this.url).get();
            this.blogLists = this.webContent.select(BLOG_LISTS_TOTAL);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean init(String url) {
        this.url = url;
        init();
        return true;
    }

    public int getBlogNum() {
        if (this.blogLists != null) {
            return this.blogLists.size();
        }
        return 0;
    }

    public ArrayList<BlogItem> getBlogList() {
        if (this.blogLists != null) {
            ArrayList<BlogItem> _blogList = new ArrayList<BlogItem>();

            // Add each blog item into blog list
            for (Element each : blogLists) {

                Document _tempDoc = Jsoup.parse(each.toString());
                Elements _tempEles = _tempDoc.select("a");
                BlogItem _blogItem = new BlogItem();

                // url address
                // System.out.println(_tempEles.attr("href"));
                _blogItem.setUrlAddress(_tempEles.attr("href"));

                // title (part one )
                // System.out.println(_tempEles.text());
                String _str = Jsoup.parse(_tempEles.attr("title")).text();
                _blogItem.setTitle(parseTitleAndPartSection(_str));
                _blogItem.setPartSection(getPartSection(_str));

                // create time
                // System.out.println(_tempDoc
                // .select("span[class=atc_tm SG_txtc]").text());
                int _ct[] = parseCreateTime(_tempDoc.select(
                        "span[class=atc_tm SG_txtc]").text());
                _blogItem.setCreateTime(new CreateTime(_ct));

                // commont number
                // System.out.println(_tempDoc.select("span[class=atc_data]"));

                // id
                // System.out.println(_tempDoc.select("span").attr("id"));
                _blogItem.setId(_tempDoc.select("span").attr("id"));

                _blogList.add(_blogItem);

            }

            return _blogList;
        }
        return null;
    }

    public int[] parseCreateTime(String text) {
        // TODO Auto-generated method stub
        if (text != null) {
            Pattern pattern = Pattern
                    .compile("(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d) (\\d\\d):(\\d\\d)");
            // IELTS Speaking Part 1: Sleeping topic
            Matcher matcher = pattern.matcher(text);
            // System.out.println(text + pattern);
            if (matcher.find()) {
                // System.out.println("inter = " + matcher.group());
                return new int[]{Integer.parseInt(matcher.group(1)),
                        Integer.parseInt(matcher.group(2)),
                        Integer.parseInt(matcher.group(3)),
                        Integer.parseInt(matcher.group(4)),
                        Integer.parseInt(matcher.group(5))};
            }
        }
        return null;
    }

    // This method is used for update
    public String parseTitleAndPartSection(String text) {
        // TODO Auto-generated method stub
        if (text != null) {
            Pattern pattern = Pattern.compile(".*:(.+)$",
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                return matcher.group(1).trim();
            }
            String irregularStr = text.toLowerCase();
            return irregularStr.substring(irregularStr.lastIndexOf("part") + 6,
                    irregularStr.length());
        }
        return null;
    }

    private PartSection getPartSection(String title) {
        if (title != null) {
            if (title.toLowerCase().contains("1")) {
                return PartSection.PART_1;
            } else if (title.toLowerCase().contains("2")) {
                return PartSection.PART_2;
            } else {
                return PartSection.PART_3;
            }
        }
        return null;
    }

}
