/* This is the secton of part object because each blog item can be devided
into different parts, like some questions are from part 1,
while some are from part two, and some are from part 3*/
package net.wcip.zhihonglu.johnieltsspeaking.partsection;

public enum PartSection {

	PART_1("Part 1"), PART_2("Part 2"), PART_3("Part 3");

	private String section;

	PartSection(String section) {
		// TODO Auto-generated constructor stub
		this.section = section;
	}

	public String getSection() {
		return section;
	}

}
